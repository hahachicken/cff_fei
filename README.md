# files
```
.
├── contracts
│   ├── core
│   │   ├── CFController.sol     : new contract, to be deployed
│   │   ├── CFPool.sol           : new contract, to be deployed
│   │   ├── CFVault.sol          : reference to existing contract
│   │   ├── CRVExchange.sol      : reference to existing contract
│   │   └── old_CFController.sol : reference to existing contract
│   ├── erc20        : reference to existing contract
│   ├── utils        : reference to existing contract
│   ├── Agent.sol    : local test contract
│   ├── DummyFei.sol : local test contract
│   └── Upgrader.sol : new contract, to be deployed
├── scripts
│   ├── helper.py
│   ├── simulate.py
│   └── upgrade.py : the deploy & upgrade script on mainnet
├── tests
│   ├── test_dummyfei.py
│   ├── test_earnReward.py
│   └── test_upgrade.py
├── abi.db : sqlite database, cache abi for faster test
├── brownie-config.yaml
└── README.md
```

# tests
- [x] brownie test tests/test_dummyfei.py   -s --network mainnet-fork --interactive
- [x] brownie test tests/test_upgrade.py    -s --network mainet-fork --interactive
- [x] brownie test tests/test_earnReward.py -s --network mainnet-fork --interactive
- [x] brownie test tests/test_mix.py -s --network mainnet-fork --interactive