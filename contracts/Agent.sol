pragma solidity >=0.4.21 <0.6.0;

contract Agent {

    constructor () public {
    }

    // to be used for ERC20
    function exec(address callee, bytes calldata payload) external returns (bytes memory)  {
        (bool success, bytes memory returnData) = address(callee).call(payload);
        require(success, "callee return failed when executing payload");
        return returnData;
    }
    
    // fallback function for receive ETH
    event ReceiveETH(uint256);
    function() external payable {
        emit ReceiveETH(msg.value);
    }

    // to be used for ETH
    function exec(address callee, uint256 ETH_amount, bytes calldata payload) external payable returns (bytes memory) {
        (bool success, bytes memory returnData) = address(callee).call.value(ETH_amount)(payload);
        require(success, "callee return failed when executing payload");
        return returnData;
    }


}