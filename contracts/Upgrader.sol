pragma solidity >=0.4.21 <0.6.0;
import "./utils/Address.sol";
import "./utils/Ownable.sol";
import "./erc20/IERC20.sol";
import "./core/CFVault.sol";
import "./core/CFController.sol";
import "./core/old_CFController.sol";

contract IVault is Ownable{
    function changeController(address) public;
}

contract IController is Ownable{
    function deposit(uint256) public;
    function withdraw(uint256) public;
    function setVault(address) public;
    function get_current_pool() public view returns(ICurvePool);
}

contract Upgrader is Ownable {
    using Address for address;

    address new_owner;
    IVault vault;
    IController old_controller;
    IController new_controller;
    ICurvePool new_pool;
    ICurvePool old_pool;

    constructor(address owner) public  {
        transferOwnership(owner);
    }

    function prepare_upgrade(address _vault, address _old_controller, address _new_controller) public onlyOwner() {
        vault = IVault(_vault.toPayable());
        old_controller = IController(_old_controller.toPayable());
        old_pool = old_controller.get_current_pool();
        new_controller = IController(_new_controller.toPayable());
        new_pool = new_controller.get_current_pool();
        new_owner = this.owner();
    }

    event withdrawn(uint256 tt_amount);
    event minted(uint256 lp_amount);
    /**
     * @notice make sure this is [vault, old_controller, new_controller, new pool]'s owner
     */
    function upgrade() public onlyOwner() {
        IERC20 target_token = IERC20(0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48);
        
        old_pool.setController(address(old_controller), address(this)); //actually set vault
        old_controller.setVault(address(this));
        // start withdraw
        old_controller.withdraw(old_pool.get_lp_token_balance());

        uint256 target_token_amount = target_token.balanceOf(address(this));
        emit withdrawn(target_token_amount);

        //@Here: all asset in this as USDC
        new_pool.setController(address(new_controller), address(this));
        new_controller.setVault(address(this));
        target_token.transfer(address(new_pool), target_token_amount);
        new_controller.deposit(target_token_amount);
        emit minted(new_pool.get_lp_token_balance());
        
        // @Here: all asset in controller.pool as lp

        // clean up
        new_pool.setController(address(new_controller), address(vault));
        new_controller.setVault(address(vault));
        vault.changeController(address(new_controller));
        // release owner ship
        releaseOwnership(new_owner);
    }

    function releaseOwnership(address _owner) public onlyOwner() {
        vault.transferOwnership(_owner);
        new_controller.transferOwnership(_owner);
        Ownable(address(new_pool)).transferOwnership(_owner);
        old_controller.transferOwnership(_owner);
        Ownable(address(old_pool)).transferOwnership(_owner);
    }
}