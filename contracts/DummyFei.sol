pragma solidity >=0.4.21 <0.6.0;
import "./erc20/IERC20.sol";
import "./erc20/ERC20Impl.sol";

contract DummyFei is ERC20Base{

    constructor () public ERC20Base(
        ERC20Base(0x0),
        0,
        "dummyFei Lp",
        18,
        "DFLP",
        true){
        //Contract.from_explorer("0xBFB6f7532d2DB0fE4D83aBb001c5C2B0842AF4dB")
        //_generateTokens(address(this), 10000000_000000000000000000);
    }
    
    IERC20 public curve_f3_lp_token = IERC20(address(0x06cb22615BA53E60D67Bf6C341a0fD5E718E1655));

    event balanceOfUnderlying_Event();
    function balanceOfUnderlying(address owner) public returns(uint256) {
        emit balanceOfUnderlying_Event();
        return balanceOf(owner);
    }


    function mint(uint256 mintAmount) public {
        if(totalSupply() == 0) {
            _generateTokens(msg.sender, mintAmount);
        } else {
            uint256 mint_lp_amount = mintAmount*(totalSupply())/curve_f3_lp_token.balanceOf(address(this));
            _generateTokens(msg.sender, mint_lp_amount);
        }
        curve_f3_lp_token.transferFrom(msg.sender, address(this), mintAmount);
    }

    function redeemUnderlying(uint256 redeemAmount) public {
        uint256 redeem_lp_amount = redeemAmount*(curve_f3_lp_token.balanceOf(address(this)))/totalSupply();
        _destroyTokens(msg.sender, redeemAmount);
        curve_f3_lp_token.transfer(msg.sender, redeem_lp_amount);
    }

    // to be used for ERC20
    function exec(address callee, bytes calldata payload) external returns (bytes memory)  {
        (bool success, bytes memory returnData) = address(callee).call(payload);
        require(success, "callee return failed when executing payload");
        return returnData;
    }
    
    // fallback function for receive ETH
    event ReceiveETH(uint256);
    function() external payable {
        emit ReceiveETH(msg.value);
    }

    // to be used for ETH
    function exec(address callee, uint256 ETH_amount, bytes calldata payload) external payable returns (bytes memory) {
        (bool success, bytes memory returnData) = address(callee).call.value(ETH_amount)(payload);
        require(success, "callee return failed when executing payload");
        return returnData;
    }


}