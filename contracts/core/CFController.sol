pragma solidity >=0.4.21 <0.6.0;

import "../utils/Ownable.sol";
import "../utils/Address.sol";
import "../erc20/SafeERC20.sol";
import "../utils/AddressArray.sol";
import "../utils/SafeMath.sol";
import "../utils/TransferableToken.sol";
import "./CFPool.sol";
import "./CRVExchange.sol";


contract CFControllerV3 is Ownable {
    using SafeERC20 for IERC20;
    using TransferableToken for address;
    using AddressArray for address[];
    using SafeMath for uint256;
    using Address for address;

    address public pool;

    uint256 public last_earn_block;
    uint256 public earn_gap;
    address public target_token;
    address[] public yield_tokens;

    address public fee_pool;
    uint256 public harvest_fee_ratio;
    uint256 public ratio_base;


    YieldHandlerInterface public yield_handler;

    address public vault;
    address weth = address(0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2);

    //@param _target, when it's 0, means ETH
    constructor(address _target, uint256 _earn_gap) public{
        last_earn_block = 0;
        require(_target != address(0x0), "invalid target address");
        require(_earn_gap != 0, "invalid earn gap");
        target_token = _target;
        earn_gap = _earn_gap;
        ratio_base = 10000;
    }

    function setVault(address _vault) public onlyOwner{
        require(_vault != address(0x0), "invalid vault");
        vault = _vault;
    }

    modifier onlyVault{
        require(msg.sender == vault, "only vault can call this");
        _;
    }

    function get_current_pool() public view returns(ICurvePool) {
        return ICurvePool(pool);
    }

    function deposit(uint256 _amount) public onlyVault {
        ICurvePool(pool).deposit(_amount);
    }

    function withdraw(uint256 _amount) public onlyVault{
        ICurvePool(pool).withdraw(_amount);
    }

    event EarnExtra(address addr, address token, uint256 amount);
    /**
    * @param min_amount in target token
    * @notice least min_amount blocks to call this
    */
    function earnReward(uint min_amount) public onlyOwner{
        require(yield_handler != YieldHandlerInterface(0x0), "invalid yield handler");
        require(block.number.safeSub(last_earn_block) >= earn_gap, "not long enough");
        last_earn_block = block.number;

        // call earn yield here
        ICurvePool(pool).earnReward(yield_tokens);
        // @here: yield tokens at controller
    
        // swap: yield tokens -> target token
        for(uint i = 0; i < yield_tokens.length; i++){
            uint256 amount = IERC20(yield_tokens[i]).balanceOf(address(this));
            if(amount > 0){
                IERC20(yield_tokens[i]).approve(address(yield_handler), amount);
                if(target_token == address(0x0)){
                    yield_handler.handleExtraToken(yield_tokens[i], weth, amount, min_amount);
                }else{
                    yield_handler.handleExtraToken(yield_tokens[i], target_token, amount, min_amount);
                }
            }
        }

        // @here: target tokan at controller

        uint256 amount = TransferableToken.balanceOfAddr(target_token, address(this));
        _refundTarget(amount);
    }

    event CFFRefund(uint256 amount, uint256 fee);
    function _refundTarget(uint256 _amount) internal {
        if(_amount == 0){
            return ;
        }
        if(harvest_fee_ratio != 0 && fee_pool != address(0x0)){
            uint256 f = _amount.safeMul(harvest_fee_ratio).safeDiv(ratio_base);
            emit CFFRefund(_amount, f);
            _amount = _amount.safeSub(f);
            if(f != 0){
                TransferableToken.transfer(target_token, fee_pool.toPayable(), f);
            }
        } else {
            emit CFFRefund(_amount, 0);
        }
        TransferableToken.transfer(target_token, pool.toPayable(), _amount);
        ICurvePool(pool).deposit(_amount);
    }

    function pause() public onlyOwner{
        pool = address(0x0);
    }

    event AddYieldToken(address _new);
    function addYieldToken(address _new) public onlyOwner{
        require(_new != address(0x0), "invalid extra token");
        yield_tokens.push(_new);
        emit AddYieldToken(_new);
    }

    event RemoveYieldToken(address _addr);
    function removeYieldToken(address _addr) public onlyOwner{
        require(_addr != address(0x0), "invalid address");
        uint len = yield_tokens.length;
        for(uint i = 0; i < len; i++){
            if(yield_tokens[i] == _addr){
                yield_tokens[i] = yield_tokens[len - 1];
                yield_tokens[len - 1] =address(0x0);
                yield_tokens.length = len - 1;
                emit RemoveYieldToken(_addr);
                return;
            }
        }
    }

    event ChangeYieldHandler(address old, address _new);
    function changeYieldHandler(address _new) public onlyOwner{
        address old = address(yield_handler);
        yield_handler = YieldHandlerInterface(_new);
        emit ChangeYieldHandler(old, address(yield_handler));
    }

    event ChangePool(address old, address _new);
    function changePool(address _p) public onlyOwner{
        address old = pool;
        pool = _p;
        emit ChangePool(old, pool);
    }

    event ChangeFeePool(address old, address _new);
    function changeFeePool(address _fp) public onlyOwner{
        address old = fee_pool;
        fee_pool = _fp;
        emit ChangeFeePool(old, fee_pool);
    }

    event ChangeHarvestFee(uint256 old, uint256 _new);
    function changeHarvestFee(uint256 _fee) public onlyOwner{
        require(_fee < ratio_base, "invalid fee");
        uint256 old = harvest_fee_ratio;
        harvest_fee_ratio = _fee;
        emit ChangeHarvestFee(old, harvest_fee_ratio);
    }

    function() external payable{}
}
