from brownie import *
from scripts.helper import ContractsManager, setup_user

def deposit(user = accounts[0]):
    cm = ContractsManager()
    setup_user()

    USDC_contract = cm.get("USDC_contract")

    # deposit to curve fei.money
    curve_deposit_gate = cm.get("curve.deposit_gate")
    curve_lp_token = cm.get("curve.metapool.fei")
    USDC_contract.approve(curve_deposit_gate, USDC_contract.balanceOf(user), { 'from': user })
    curve_deposit_gate.add_liquidity(cm.get("curve.metapool.fei"), [0, 0, USDC_contract.balanceOf(user), 0], 0, { "from": user })
    
    delegator = cm.get("fei.delegator")
    
    curve_lp_token.approve(delegator, curve_lp_token.balanceOf(user), { 'from': user })
    delegator.mint(curve_lp_token.balanceOf(user), { 'from': user })
    print(curve_lp_token.balanceOf(user))
    #Delegator.redeem(curve_lp_token.address, { 'from': user })

def claim(user = accounts[0]):
    rewards_distributor = cm.get("fei.rewards_distributor")
    rewards_distributor.claimRewards([user], ["0xBFB6f7532d2DB0fE4D83aBb001c5C2B0842AF4dB"], 
        False, True, {"from": user}
    )

def withdraw(user = accounts[0]):
    delegator = cm.get("fei.delegator")
    delegator.redeemUnderlying(delegator.balanceOf(user), {'from': user})