from eth_account import Account
from brownie import *
from scripts.helper import ContractsManager
from brownie.network import gas_price, priority_fee

def deploy():
    operator = accounts.load("ENF_operator")
    owner = "0xc3Fd2bcB524af31963b3E3bB670F28bA14718244"
    cm = ContractsManager()
    if network.show_active() != "mainnet-fork":
        priority_fee("auto")

    SafeMath.deploy({"from": operator}, publish_source=True)
    TransferableToken.deploy({"from": operator}, publish_source=True)

    upgrader = Upgrader.deploy(owner, {"from": operator}, publish_source=True)
    new_pool = CFPoolV3.deploy(cm.get("fei.delegator"), cm.get("fei.rewards_distributor"), {"from": operator}, publish_source=True)
    new_controller = CFControllerV3.deploy(cm.get("USDC_contract"), 1, {"from": operator}, publish_source=True)
    # setup
    new_controller.addYieldToken(cm.get("fei.tribe", {"from": operator}))
    new_controller.changePool(new_pool, {"from": operator})
    new_controller.changeYieldHandler(cm.get("enf.yield_handler"), {"from":operator})
    new_pool.setController(new_controller, {'from': operator})
    # transfer owner
    new_controller.transferOwnership(upgrader, {'from': operator})
    new_pool.transferOwnership(upgrader, {'from': operator})

# done by owner
def upgrade():
    owner = "0xc3Fd2bcB524af31963b3E3bB670F28bA14718244"
    cm = ContractsManager()

    # yield_handler add path tribe->WETH->USDC
    yield_handler = cm.get("enf.yield_handler")
    yield_handler.addPath(cm.get("dex.uniV2"), [cm.get("fei.tribe"), "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2", cm.get("USDC_contract")], {"from": owner})

    # prepare upgrade
    upgrader.prepare_upgrade(       
        cm.get("enf.vault"),
        cm.get("enf.controllerV2"),
        new_controller,
        new_pool,
        {"from": owner}
    )
    vault = cm.get("enf.vault")
    vault.transferOwnership(upgrader, {"from": owner})
    old_controller = cm.get("enf.controllerV2")
    old_controller.transferOwnership(upgrader, {"from": owner})
    # actual upgrade
    upgrader.upgrade({"from": owner})