import sqlite3
import json
from brownie import *

class ContractsManager:
    def __init__(self, db_name = "abi.db"):
        self._con = sqlite3.connect('abi.db')
    
    def __del__(self):
        self._con.commit()
        self._con.close()
    
    def initDB(self):
        cur = self._con.cursor()
        cur.execute("CREATE TABLE eth_main_net (name text, address text, abi_json text, PRIMARY KEY (name, address))")
    
    def get(self, name, address = None):
        '''
        prioritize name > address 
        '''
        cur = self._con.cursor()
    
        cur.execute("SELECT * FROM eth_main_net WHERE name = '%s'" % name)
        re = cur.fetchone()

        if re is None:
            # get by address
            if not address is None:
                cur.execute("SELECT * FROM eth_main_net WHERE address = '%s'" % address)
                re = cur.fetchone()

                if re is None:
                    # get from explorer
                    contract = Contract.from_explorer(address)
                    cur.execute("INSERT INTO eth_main_net(name, address, abi_json) values(?, ?, ?)", 
                        (name, contract.address, json.dumps(contract.abi))
                    )
                    self._con.commit()
                    return contract

                # check is name matches, update name if not
                if re[0] != name:
                    cur.execute("DELETE FROM eth_main_net WHERE address = '%s'" % address)
                    cur.execute("INSERT INTO eth_main_net(name, address, abi_json) values(?, ?, ?)", 
                        (name, re[1], re[2])
                    )
                    self._con.commit()
                    cur.execute("SELECT * FROM eth_main_net WHERE address = '%s'" % address)
                    re = cur.fetchone()
            else:
                return None
        return Contract.from_abi(re[0], re[1], json.loads(re[2]))
    
    def cache(self):
        self.get("dex.sushi", "0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F")
        self.get("dex.uniV2", "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D")
        self.get("dex.uniV3", "0xE592427A0AEce92De3Edee1F18E0157C05861564")
        
        self.get("USDC_contract", "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48")
        
        self.get("curve.deposit_gate", "0xA79828DF1850E8a3A3064576f380D90aECDD3359")
        self.get("curve.metapool.fei", "0x06cb22615BA53E60D67Bf6C341a0fD5E718E1655")

        self.get("fei.delegator", "0xBFB6f7532d2DB0fE4D83aBb001c5C2B0842AF4dB")
        self.get("fei.rewards_distributor", "0x73F16f0c0Cd1A078A54894974C5C054D8dC1A3d7")
        self.get("fei.tribe", "0xc7283b66Eb1EB5FB86327f08e1B5816b0720212B")

        self.get("enf.vault", "0x889B9194Fb1D66509d3d043e7c839582fED6E607")
        self.get("enf.lp_token", "0x412EbDc655f897e0eC0f89022bc7DEC62BAaE0aF")
        self.get("enf.controllerV2", "0x85DC869dA0042b60Bc466D8e95bf477bEDD89Ed7")
        self.get("enf.yield_handler", "0x064c0e96d52a640dFE4e427F820Ba4d0009176E8")

        self.get("enf.vault.test", "0x0c435F087980E58d79e9a74f0547Ed9cd0b41b18")
        self.get("enf.lp_token.test", "0xb9a49dD673F97B44496c7130A5B44cBB8600685f")
        self.get("enf.controllerV2.test", "0x6495bFeee28Ce9A16402bC56a6e31B1884485309")

        self.get("enf.controllerV3", "0x39EAc440D26ef29270Dc5bD91FB6A63FE282Af0d")
        self.get("enf.poolV3", "0xeC66C6e9De4c474EC13e7dD122C7658AF830CD52")
        self.get("enf.upgrader", "0x7B64676b36cbF9A02E7cc171A35bE3AfA3dD4E2c")

cm = ContractsManager()

def setup_user(user = None, amount = 1000_000000):
    if network.show_active() != "mainnet-fork" and user == None:
        user = accounts[0]
    dex = cm.get("dex.sushi")
    dex.swapETHForExactTokens(
        amount,
        # eth -> usdc
        ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2", "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48"], 
        user,
        chain.time() + 10800, 
        {"from": user, "value": user.balance()}
    )