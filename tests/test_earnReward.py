import pytest
from brownie import *
if not network.is_connected():
    network.connect('mainnet-fork')
from scripts.helper import ContractsManager, setup_user
from test_dummyfei import deploy_dummy_fei
from test_upgrade import deploy_new_contracts

cm = ContractsManager()
user = accounts[0]
operator = accounts[3]
owner = accounts[-1]
holder = accounts[-2]

@pytest.fixture
def safemath():
    s = SafeMath.deploy({"from": accounts[0]})
    return s
@pytest.fixture
def transferableToken():
    s = TransferableToken.deploy({"from": accounts[0]})
    return s

@pytest.mark.require_network("mainnet-fork")
def test(safemath, transferableToken):
    print("test start")

    vault = cm.get("enf.vault")
    old_controller = cm.get("enf.controllerV2")
    old_pool = Contract.from_explorer(old_controller.get_current_pool())
    new_controller, new_pool, upgrader, yield_handler = deploy_new_contracts()
    print("new contracts deployed")
    
    print("starting upgrade")
    for c in [vault, old_controller, old_pool, new_controller, new_pool]:
        c.transferOwnership(upgrader, {"from": c.owner()})

    upgrader.prepare_upgrade(       
        vault,
        old_controller,
        new_controller,
        {"from": owner}
    )
    upgrader.upgrade({"from": owner})

    # @ HERE upgrade complete, 
    # test on vault with new controller & pool
    print("upgrade complete")
    USDC_contract = cm.get("USDC_contract")
    enf_lp_token = cm.get("enf.lp_token")
    setup_user(user)

    print("test earnReward")
    # transfer tribe from top holder, pretend as earned
    tribe = cm.get("fei.tribe")
    tribe.transfer(new_pool, tribe.balanceOf(holder)/2, {'from': holder})
    print(vault.get_asset())
    new_controller.earnReward(0, {'from': owner})
    print(vault.get_asset())
   
    # ------

    print("starting downgrade")
    for c in [vault, old_controller, old_pool, new_controller, new_pool]:
        c.transferOwnership(upgrader, {"from": c.owner()})
    upgrader.prepare_upgrade(       
        vault,
        new_controller,
        old_controller,
        {"from": owner}
    )
    upgrader.upgrade({"from": owner})
    print("downgrade complete")
    # @ HERE upgrade complete, 

    print("test earnReward")
    print(vault.get_asset())
    chain.mine(1024)
    chain.sleep(1024)
    new_controller.earnReward(0, {'from': owner})
    print(vault.get_asset())