import pytest
from brownie import *
if not network.is_connected():
    network.connect('mainnet-fork')
from scripts.helper import ContractsManager, setup_user

cm = ContractsManager()
operator = accounts[3]
owner = accounts[-1]

def deploy_dummy_fei():
    dummy_fei = DummyFei.deploy({"from": operator})
    fei_delegator = cm.get("fei.delegator")
    holder = accounts[-2]
    fei_delegator.transfer(dummy_fei, fei_delegator.balanceOf(holder), {"from": holder})
    return dummy_fei

@pytest.mark.require_network("mainnet-fork")
def test():
    print("test setting up")
    user = accounts[0]
    setup_user(user)
    USDC_contract = cm.get("USDC_contract")


    print("depositing to curve.fei.money pool")
    curve_deposit_gate = cm.get("curve.deposit_gate")
    curve_lp_token = cm.get("curve.metapool.fei")
    USDC_contract.approve(curve_deposit_gate, USDC_contract.balanceOf(user), { 'from': user })
    curve_deposit_gate.add_liquidity(cm.get("curve.metapool.fei"), [0, 0, USDC_contract.balanceOf(user), 0], 0, { "from": user })
    
    delegator = cm.get("fei.delegator")
    dummy_fei = deploy_dummy_fei()
    chain.mine(2)

    print("deposit & withdraw")
    print(curve_lp_token.balanceOf(user), dummy_fei.balanceOf(user))
    curve_lp_token.approve(dummy_fei, curve_lp_token.balanceOf(user), { 'from': user })
    dummy_fei.mint(curve_lp_token.balanceOf(user), { 'from': user })
    print(curve_lp_token.balanceOf(user), dummy_fei.balanceOf(user))
    dummy_fei.redeemUnderlying(dummy_fei.balanceOf(user), {'from' :user})
    print(curve_lp_token.balanceOf(user), dummy_fei.balanceOf(user))

    print("deposit & withdraw * 2")
    curve_lp_token.approve(dummy_fei, curve_lp_token.balanceOf(user), { 'from': user })
    dummy_fei.mint(curve_lp_token.balanceOf(user)/2, { 'from': user })
    print(curve_lp_token.balanceOf(user), dummy_fei.balanceOf(user))
    dummy_fei.redeemUnderlying(dummy_fei.balanceOf(user)/2, {'from' :user})
    print(curve_lp_token.balanceOf(user), dummy_fei.balanceOf(user))

    print("deposit & withdraw * 3")
    curve_lp_token.approve(dummy_fei, curve_lp_token.balanceOf(user), { 'from': user })
    dummy_fei.mint(curve_lp_token.balanceOf(user), { 'from': user })
    print(curve_lp_token.balanceOf(user), dummy_fei.balanceOf(user))
    dummy_fei.redeemUnderlying(dummy_fei.balanceOf(user), {'from' :user})
    print(curve_lp_token.balanceOf(user), dummy_fei.balanceOf(user))